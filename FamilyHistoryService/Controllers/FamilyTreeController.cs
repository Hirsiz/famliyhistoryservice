using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FamilyHistoryService.Models;
using FamilyHistoryService.Models.D3Tree;
using FamilyHistoryService.Models.Family;
using SubmaintainerRequestModel;

[Route("api/[controller]")]
[ApiController]
public class FamilyTreeController : ControllerBase
{
    public readonly IFamilyTreeManager __FamilyTreeManager;
    public readonly IFamilyModificationManager __FamilyModificationManager;
    public readonly IMergeTreeManager __MergeTreeManager;
    public readonly IMaintainerManager __MaintanerManager;


    public FamilyTreeController(
        IFamilyTreeManager familyTreeManager,
        IFamilyModificationManager familyModificationManager,
        IMergeTreeManager mergeTreeManager,
        IMaintainerManager maintainerManager)
    {
        __FamilyTreeManager = familyTreeManager;
        __FamilyModificationManager = familyModificationManager;
        __MergeTreeManager = mergeTreeManager;
        __MaintanerManager = maintainerManager;
    }

    // GET api/values
    [HttpGet]
    public ActionResult<D3IndividualModel> Get(string treeid)
    {
        return __FamilyTreeManager.GetD3Records(treeid);
    }

    // GET api/values
    [HttpGet]
    [Route("GetAllFamilies")]
    public ActionResult<List<Record>> GetAllFamilies(string userID)
    {
        return __FamilyTreeManager.GetRecords(userID);
    }

    [HttpGet]
    [Route("CheckMaintainerPriviliges")]
    public ActionResult<bool> CheckMaintainerPriviliges(string userid, string treeid, string accessToken)
    {
        return __FamilyModificationManager.CheckMaintainerPriviliges(userid, treeid, accessToken);
    }

    [HttpPost]
    public ActionResult<bool> Post(IndividualEditRequestModel requestModel)
    {
        return __FamilyModificationManager.EditRecord(requestModel);
    }

    [HttpPost]
    [Route("AddIndividual")]
    public ActionResult<bool> AddIndividual(IndividualAddRequestModel requestModel)
    {
        return __FamilyModificationManager.AddRecord(requestModel);
        // return true;
    }

    [HttpPost]
    [Route("MergeTreeByAdd")]
    public ActionResult<bool> MergeTreeByAdding(TreeAddRequestModel requestModel)
    {
        return __MergeTreeManager.MergeTreeByAdd(requestModel);
    }

    [HttpPost]
    [Route("MergeTreeByReplace")]
    public ActionResult<bool> MergeTreeByReplace(TreeAddRequestModel requestModel)
    {
        return __MergeTreeManager.MergeTreeByReplace(requestModel);
    }

    [HttpGet]
    [Route("GetFamilies")]
    public ActionResult<IEnumerable<FamilyModel>> GetFamilies(string id)
    {
        return __MergeTreeManager.GetFamilies(id);
    }

    [HttpPost]
    [Route("AddSubmaintainer")]
    public ActionResult<bool> AddSubmaintainer(Submaintainer request)
    {
        return __MaintanerManager.StoreSubmaintainer(request);
    }

    [HttpGet]
    [Route("GetSubmaintainers")]
    public ActionResult<Submaintainer> GetSubmaintainers(string treeid)
    {
        return __MaintanerManager.GetSubmaintainersForTree(treeid);
    }
}