public class FamilyTreeDatabaseSettings : IFamilyTreeDatabaseSettings
{
    public string FamilyTreeCollectionName { get; set; }
    public string UserGroupCollectionName { get; set; }
    public string SubmaintainerCollectionName { get; set; }
    public string ConnectionString { get; set; }
    public string DatabaseName { get; set; }
}