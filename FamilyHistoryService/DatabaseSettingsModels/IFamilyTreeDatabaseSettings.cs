public interface IFamilyTreeDatabaseSettings
{
    string FamilyTreeCollectionName { get; set; }
    string UserGroupCollectionName { get; set; }
    string SubmaintainerCollectionName {get;set;}
    string ConnectionString { get; set; }
    string DatabaseName { get; set; }
}