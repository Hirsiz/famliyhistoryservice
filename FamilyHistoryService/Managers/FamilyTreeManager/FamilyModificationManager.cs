using System.Collections.Generic;
using System.Linq;
using FamilyHistoryService.Models;
using FamilyHistoryService.Models.Individual;
public class FamilyModificationManager : IFamilyModificationManager
{
    public IRecordRepository __RecordRepository;
    public IUserGroupRepository __UserGroupRepository;


    public FamilyModificationManager(IRecordRepository recordRepository, IUserGroupRepository userGroupRepository)
    {
        __RecordRepository = recordRepository;
        __UserGroupRepository = userGroupRepository;
    }

    public bool EditRecord(IndividualEditRequestModel requestModel)
    {
        return __RecordRepository.UpdateIndividual(requestModel);
    }

    public bool AddRecord(IndividualAddRequestModel requestModel)
    {
        return __RecordRepository.AddIndividualToFamily(requestModel);
    }

    public bool CheckMaintainerPriviliges(string userid, string treeid, string accessToken)
    {

        Record _Record = __RecordRepository.Get(treeid);
        if (_Record.MaintainerGroupID == "0")
        {
            return _Record.AccessToken.ToString() == accessToken;
        }
        else
        {
            UserGroupModel _MaintainerGroup = __UserGroupRepository.Get(_Record.MaintainerGroupID);
            return _MaintainerGroup.Users.Any(user => user.Id == userid);

        }
    }

}