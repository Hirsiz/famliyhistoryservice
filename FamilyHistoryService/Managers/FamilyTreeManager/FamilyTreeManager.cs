using System.Collections.Generic;
using System.Linq;
using FamilyHistoryService.Models;
using FamilyHistoryService.Models.D3Tree;
using FamilyHistoryService.Models.Family;

public class FamilyTreeManager : IFamilyTreeManager
{
    private IRecordRepository __RecordRepository;
    private IUserGroupRepository __UserGroupRepository;

    public FamilyTreeManager(IRecordRepository recordRepository, IUserGroupRepository userGroupRepository)
    {
        __RecordRepository = recordRepository;
        __UserGroupRepository = userGroupRepository;
    }

    public List<Record> GetRecords(string userID)
    {
        List<UserGroupModel> _UserGroupModel = __UserGroupRepository.Get();
        List<Record> _Records = __RecordRepository.Get();
        List<Record> _VisibleRecords = new List<Record>();
        foreach (Record record in _Records)
        {
            UserGroupModel _RecordUserGroup = _UserGroupModel.Where(userGroup => userGroup.Id == record.VisibilityGroupID).SingleOrDefault();

            if (_RecordUserGroup != null && _RecordUserGroup.Users.Any(user => user.Id == userID))
            {
                _VisibleRecords.Add(record);
            }
        }

        _VisibleRecords.AddRange(_Records.Where(record => record.VisibilityGroupID == "0").ToList());

        return _VisibleRecords;
    }

    public D3IndividualModel GetD3Records(string id)
    {

        return ConvertRecordsToD3Trees(__RecordRepository.Get(id));
    }

    private D3IndividualModel ConvertRecordsToD3Trees(Record record)
    {
        D3IndividualModel _D3Trees = ConvertRecordToD3Tree(record);
        return _D3Trees;
    }

    private D3IndividualModel ConvertRecordToD3Tree(Record record)
    {
        D3IndividualModel _Individual = new D3IndividualModel();
        FamilyModel _RootFamily = record.Family[0];
        //we are going to make an assumption that the father is the ancestor
        //need to adapt this later

        _Individual.Name = _RootFamily.Father.Name;
        _Individual.Class = "man";
        _Individual.Extra.DateOfBirth = _RootFamily.Father.Birth.Date;
        _Individual.Extra.DateOfDeath = _RootFamily.Father.Death.Date;
        _Individual.Extra.ID = _RootFamily.Father.ID.ToString();
        _Individual.Extra.SubMaintainerIDs = _RootFamily.SubMaintainerIDs;

        _Individual.Marriages = new List<Marriage>()
        {
            new Marriage(){
                Spouse = new D3IndividualModel(){
                    Name = _RootFamily.Mother.Name,
                    Class = "woman",
                    Extra = new Extra()
                    {
                        DateOfBirth = _RootFamily.Mother.Birth.Date,
                        DateOfDeath = _RootFamily.Mother.Death.Date,
                        ID = _RootFamily.Mother.ID.ToString(),
                        SubMaintainerIDs = _RootFamily.SubMaintainerIDs
                    }
                },
                Children = ProcessChildren(_RootFamily)
            }
        };

        return _Individual;
    }

    private List<D3IndividualModel> ProcessChildren(FamilyModel family)
    {
        List<D3IndividualModel> _Children = new List<D3IndividualModel>();
        foreach (FamilyModel childrenFamily in family.ChildrenFamily)
        {
            D3IndividualModel _Individual = new D3IndividualModel();
            if (childrenFamily.Father != null && childrenFamily.Father.IsAncestor)
            {
                _Individual.Name = childrenFamily.Father.Name;
                _Individual.Class = "man";
                _Individual.Extra.DateOfBirth = childrenFamily.Father.Birth.Date;
                _Individual.Extra.DateOfDeath = childrenFamily.Father.Death.Date;
                _Individual.Extra.ID = childrenFamily.Father.ID.ToString();
                _Individual.Extra.SubMaintainerIDs = childrenFamily.SubMaintainerIDs;

                if (childrenFamily.Mother != null)
                {
                    _Individual.Marriages = new List<Marriage>()
                    {
                        new Marriage(){
                            Spouse = new D3IndividualModel(){
                                Name = childrenFamily.Mother.Name,
                                Extra = new Extra()
                                {
                                    DateOfBirth = childrenFamily.Mother.Birth.Date,
                                    DateOfDeath = childrenFamily.Mother.Death.Date,
                                    ID = childrenFamily.Mother.ID.ToString(),
                                    SubMaintainerIDs = childrenFamily.SubMaintainerIDs
                                },
                                Class = "woman"
                            },
                            Children = ProcessChildren(childrenFamily)
                        }
                    };
                }
                _Children.Add(_Individual);
            }
            else if (childrenFamily.Mother != null && childrenFamily.Mother.IsAncestor)
            {
                _Individual.Name = childrenFamily.Mother.Name;
                _Individual.Class = "woman";
                _Individual.Extra.DateOfBirth = childrenFamily.Mother.Birth.Date;
                _Individual.Extra.DateOfDeath = childrenFamily.Mother.Death.Date;
                _Individual.Extra.ID = childrenFamily.Mother.ID.ToString();
                _Individual.Extra.SubMaintainerIDs = childrenFamily.SubMaintainerIDs;


                if (childrenFamily.Father != null)
                {
                    _Individual.Marriages = new List<Marriage>()
                    {
                        new Marriage(){
                            Spouse = new D3IndividualModel(){
                                Name = childrenFamily.Father.Name,
                                Class = "man",
                                Extra = new Extra()
                                {
                                    DateOfBirth = childrenFamily.Father.Birth.Date,
                                    DateOfDeath = childrenFamily.Father.Death.Date,
                                    ID = childrenFamily.Father.ID.ToString(),
                                    SubMaintainerIDs = childrenFamily.SubMaintainerIDs
                                },
                            },
                            Children = ProcessChildren(childrenFamily)
                        }
                    };
                }
                _Children.Add(_Individual);
            }
        }
        return _Children;
    }
}