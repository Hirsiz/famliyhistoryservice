public interface IFamilyModificationManager
{
    bool CheckMaintainerPriviliges(string userid, string treeid, string accessToken);
    bool EditRecord(IndividualEditRequestModel requestModel);
    bool AddRecord(IndividualAddRequestModel requestModel);
}