using System.Collections.Generic;
using FamilyHistoryService.Models;
using FamilyHistoryService.Models.D3Tree;

public interface IFamilyTreeManager
{
    D3IndividualModel GetD3Records(string id);
    List<Record> GetRecords(string userID);
}