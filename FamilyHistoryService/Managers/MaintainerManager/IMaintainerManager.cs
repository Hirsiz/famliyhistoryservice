using SubmaintainerRequestModel;

public interface IMaintainerManager
{
    // bool AddMaintainer(AddSubmaintainerRequestModel request);
    Submaintainer GetSubmaintainersForTree(string treeid);
    bool StoreSubmaintainer(Submaintainer subMaintainer);
}