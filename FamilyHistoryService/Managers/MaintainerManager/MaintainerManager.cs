using System;
using System.Collections.Generic;
using System.Linq;
using FamilyHistoryService.Models;
using FamilyHistoryService.Models.Family;
using SubmaintainerRequestModel;

public class MaintainerManager : IMaintainerManager
{
    public IRecordRepository __RecordRepository;
    public ISubmaintainerRepository __SubmaintainerRepository;


    public MaintainerManager(IRecordRepository recordRepository, ISubmaintainerRepository submaintainerRepository)
    {
        __RecordRepository = recordRepository;
        __SubmaintainerRepository = submaintainerRepository;
    }

    private void AddUserToSubmaintainer(FamilyModel family, string userID)
    {
        foreach (FamilyModel childrenFamily in family.ChildrenFamily)
        {
            childrenFamily.SubMaintainerIDs.Add(userID);
            AddUserToSubmaintainer(childrenFamily, userID);
        }
    }

    public Submaintainer GetSubmaintainersForTree(string treeid)
    {
        List<Submaintainer> _AllSubmaintainers = __SubmaintainerRepository.Get();
        Submaintainer _Submaintainer = _AllSubmaintainers.Where(submaintainer => submaintainer.TreeID == treeid).SingleOrDefault();
        if (_Submaintainer == null)
        {
            _Submaintainer = new Submaintainer()
            {
                TreeID = treeid
            };
        }
        return _Submaintainer;
    }

    public bool StoreSubmaintainer(Submaintainer subMaintainer)
    {
        UpdateFamiliesSubmaintainer(subMaintainer);
        List<Submaintainer> _Submaintainers = __SubmaintainerRepository.Get();
        Submaintainer _Submaintainer = _Submaintainers.Where(submaintainer => submaintainer.TreeID == subMaintainer.TreeID).SingleOrDefault();
        if (_Submaintainer == null)
        {
            __SubmaintainerRepository.Create(subMaintainer);
        }
        else
        {
            subMaintainer.Id = _Submaintainer.Id;
            __SubmaintainerRepository.Update(_Submaintainer.Id, subMaintainer);
        }

        return true;
    }

    private void RemoveAllCurrentSubmaintainers(FamilyModel family){
        family.SubMaintainerIDs = new List<string>();
        foreach(FamilyModel childrenFamily in family.ChildrenFamily){
            childrenFamily.SubMaintainerIDs =  new List<string>();
            if(childrenFamily.ChildrenFamily.Count > 0){
                RemoveAllCurrentSubmaintainers(childrenFamily);
            }
        }
    }

    private void UpdateFamiliesSubmaintainer(Submaintainer submaintainer)
    {
        Record _FamilyTree = __RecordRepository.Get(submaintainer.TreeID);

        RemoveAllCurrentSubmaintainers(_FamilyTree.Family[0]);

        foreach (UserFamilyEntity userfamily in submaintainer.UserFamilySubmaintainers)
        {
            if (userfamily.FamilyID == _FamilyTree.Family[0].ID.ToString())
            {
                if (!_FamilyTree.Family[0].SubMaintainerIDs.Contains(userfamily.UserID))
                {
                    _FamilyTree.Family[0].SubMaintainerIDs.Add(userfamily.UserID);
                }
                AddUserToSubmaintainer(_FamilyTree.Family[0], userfamily.UserID);
            }
            else
            {
                FamilyModel _Family = __RecordRepository.FindFamilyInChildFamiliesByFamilyIDForAdd(_FamilyTree.Family.Single(), userfamily.FamilyID);
                if (!_Family.SubMaintainerIDs.Contains(userfamily.UserID))
                {
                    _Family.SubMaintainerIDs.Add(userfamily.UserID);
                }
                AddUserToSubmaintainer(_Family, userfamily.UserID);
            }
        }
        __RecordRepository.Update(submaintainer.TreeID, _FamilyTree);

    }
}