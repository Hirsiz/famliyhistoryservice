using System.Collections.Generic;
using FamilyHistoryService.Models.Family;

public interface IMergeTreeManager
{
    bool MergeTreeByAdd(TreeAddRequestModel request);
    bool MergeTreeByReplace(TreeAddRequestModel request);
    List<FamilyModel> GetFamilies(string id);
}