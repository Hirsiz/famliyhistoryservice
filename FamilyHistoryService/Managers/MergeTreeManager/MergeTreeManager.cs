using System.Collections.Generic;
using System.Linq;
using FamilyHistoryService.Models;
using FamilyHistoryService.Models.Family;

public class MergeTreeManager : IMergeTreeManager
{
    public IRecordRepository __RecordRepository;

    public MergeTreeManager(IRecordRepository recordRepository)
    {
        __RecordRepository = recordRepository;
    }

    public bool MergeTreeByAdd(TreeAddRequestModel request)
    {
        List<Record> _Trees = __RecordRepository.Get();

        //Get tree 2
        Record _Tree2 = _Trees.Where(tree => tree.FileID == request.Tree2ID).SingleOrDefault();

        //get Family 1
        Record _Tree1 = _Trees.Where(tree => tree.FileID == request.Tree1ID).SingleOrDefault();

        FamilyModel _Family = __RecordRepository.FindFamilyInChildFamiliesByFamilyIDForAdd(_Tree1.Family.Single(), request.Family1ID);
        // add tree 2 as child of family 1
        FamilyModel _Tree2RootFamily = _Tree2.Family.Single();
        _Family.ChildrenFamily.Add(_Tree2RootFamily);

        __RecordRepository.Update(_Tree1.ID, _Tree1);
        // __RecordRepository.Create(_Tree1);


        return false;
    }

    public bool MergeTreeByReplace(TreeAddRequestModel request)
    {
        List<Record> _Trees = __RecordRepository.Get();

        //Get tree 2
        Record _Tree2 = _Trees.Where(tree => tree.FileID == request.Tree2ID).SingleOrDefault();

        //get Family 1
        Record _Tree1 = _Trees.Where(tree => tree.FileID == request.Tree1ID).SingleOrDefault();

        FamilyModel _ParentFamily = __RecordRepository.FindFamilyInChildFamiliesByFamilyID2ForReplace(_Tree1.Family.Single(), request.Family1ID);
        FamilyModel _Family = __RecordRepository.FindFamilyInChildFamiliesByFamilyIDForAdd(_Tree1.Family.Single(), request.Family1ID);

        // add tree 2 as child of family 1
        FamilyModel _Tree2RootFamily = _Tree2.Family.Single();
        _ParentFamily.ChildrenFamily.Add(_Tree2RootFamily);
        _ParentFamily.ChildrenFamily.Remove(_Family);

        __RecordRepository.Update(_Tree1.ID, _Tree1);
        // __RecordRepository.Create(_Tree1);


        return false;
    }

    public List<FamilyModel> GetFamilies(string id)
    {
        List<Record> _Trees = __RecordRepository.Get();
        Record _Tree = _Trees.Where(tree => tree.FileID == id).SingleOrDefault();

        List<FamilyModel> _Families = new List<FamilyModel>()
        {
            _Tree.Family.Single()
        };

        GetFamily(_Families, _Tree.Family.Single());
        return _Families;
    }

    private void GetFamily(List<FamilyModel> allFamilies, FamilyModel parentFamily)
    {
        foreach (FamilyModel family in parentFamily.ChildrenFamily)
        {
            allFamilies.Add(family);
            GetFamily(allFamilies, family);
        }
    }
}