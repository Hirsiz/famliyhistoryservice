using System.Collections.Generic;
using MongoDB.Bson;

public class Extra
{
    public string Nickname { get; set; }
    public string Age { get; set; }
    public string DateOfBirth { get; set; }
    public string DateOfDeath { get; set; }
    public string ID { get; set; }
    public List<string> SubMaintainerIDs { get; set; }
}