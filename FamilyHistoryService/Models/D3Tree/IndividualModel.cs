using System.Collections.Generic;
using MongoDB.Bson;

namespace FamilyHistoryService.Models.D3Tree
{
    public class D3IndividualModel
    {
        public ObjectId _id { get; set; }
        public string Name { get; set; }
        public string Class { get; set; }
        public string TextClass { get; set; }
        public List<Marriage> Marriages { get; set; }
        public Extra Extra { get; set; } = new Extra();
    }
}