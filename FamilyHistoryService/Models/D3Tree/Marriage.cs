using System.Collections.Generic;
using FamilyHistoryService.Models.D3Tree;

public class Marriage
{
    public D3IndividualModel Spouse { get; set; } 
    public List<D3IndividualModel> Children { get; set; }
}