using System;

namespace FamilyHistoryService.Models.Individual
{
    public class BirthModel
    {
        public string Place { get; set; }
        public string Date { get; set; }
    }
}