using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace FamilyHistoryService.Models.Individual
{
    public class IndividualModel
    {
        [BsonId]
        [BsonElement("ID")]
        public ObjectId ID { get; set; }
        public string Name { get; set; }
        public string Gender { get; set; }
        public AddressModel Address { get; set; }
        public BirthModel Birth { get; set; }
        public DeathModel Death { get; set; }
        public bool IsAncestor { get; set; }

    }
}