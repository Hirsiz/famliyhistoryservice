public class InformationModel
{
    public int MaleCount { get; set; }
    public int FemaleCount { get; set; }
    public int UnknownCount { get; set; }
}