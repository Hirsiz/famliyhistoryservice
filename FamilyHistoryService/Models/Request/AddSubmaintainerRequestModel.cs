using System.Collections.Generic;

namespace SubmaintainerRequestModel
{
    public class AddSubmaintainerRequestModel
    {
        public List<UserFamilyEntity> UserFamilySubmaintainers { get; set; }
        public string TreeID { get; set; }
    }
}