public class IndividualAddRequestModel
{
    public string TargetIndividualID {get;set;}
    public string IndividualType { get; set; }
    public string Name { get; set; }
    public string PlaceOfBirth { get; set; }
    public string DateOfBirth { get; set; }
    public string DateOfDeath { get; set; }
    public string Gender { get; set; }
}