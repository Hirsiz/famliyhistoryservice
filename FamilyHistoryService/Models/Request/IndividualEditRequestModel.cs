public class IndividualEditRequestModel{
    public string ID{get;set;}
    public string Name { get; set; }
    public string DateOfBirth { get; set; }
    public string DateOfDeath { get; set; }
}