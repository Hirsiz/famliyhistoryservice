public class TreeAddRequestModel
{
    public string Tree1ID { get; set; }
    public string Family1ID { get; set; }
    public string Tree2ID { get; set; }
    public string TreeName { get; set; }
}