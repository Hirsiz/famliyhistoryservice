using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

public class Submaintainer
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    public string Id { get; set; }
    public string TreeID { get; set; }
    public List<UserFamilyEntity> UserFamilySubmaintainers { get; set; } = new List<UserFamilyEntity>();
}