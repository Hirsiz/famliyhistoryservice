public class UserFamilyEntity
{
    public string Username { get; set; }
    public string UserID { get; set; }
    public string FamilyID { get; set; }
    public string FamilyName { get; set; }
}