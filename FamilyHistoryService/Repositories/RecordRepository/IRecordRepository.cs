using System.Collections.Generic;
using FamilyHistoryService.Models;
using FamilyHistoryService.Models.Family;

public interface IRecordRepository
{
    List<Record> Get();
    Record Get(string id);
    Record Create(Record record);
    void Update(string id, Record recordIn);
    void Remove(Record recordIn);
    void Remove(string id);
    bool UpdateIndividual(IndividualEditRequestModel requestModel);
    bool AddIndividualToFamily(IndividualAddRequestModel requestModel);
    FamilyModel FindFamilyInChildFamiliesByIndividualID(FamilyModel family, string id);
    FamilyModel FindFamilyInChildFamiliesByFamilyIDForAdd(FamilyModel family, string id);
    FamilyModel FindFamilyInChildFamiliesByFamilyID2ForReplace(FamilyModel family, string id);
}