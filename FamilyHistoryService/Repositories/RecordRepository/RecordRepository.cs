using System.Collections.Generic;
using System.Linq;
using FamilyHistoryService.Models;
using FamilyHistoryService.Models.Family;
using FamilyHistoryService.Models.Individual;
using MongoDB.Bson;
using MongoDB.Driver;

public class RecordRepository : IRecordRepository
{
    private readonly IMongoCollection<Record> __Records;

    public RecordRepository(IFamilyTreeDatabaseSettings settings)
    {
        MongoClient _Client = new MongoClient(settings.ConnectionString);
        IMongoDatabase _Database = _Client.GetDatabase(settings.DatabaseName);

        __Records = _Database.GetCollection<Record>(settings.FamilyTreeCollectionName);
    }

    public List<Record> Get() =>
            __Records.Find(record => true).ToList();

    public Record Get(string id) =>
            __Records.Find<Record>(record => record.ID == id).FirstOrDefault();

    public Record Create(Record record)
    {
        __Records.InsertOne(record);
        return record;
    }

    public void Update(string id, Record recordIn) =>
        __Records.ReplaceOne(record => record.ID == id, recordIn);

    public void Remove(Record recordIn) =>
        __Records.DeleteOne(record => record.ID == recordIn.ID);

    public void Remove(string id) =>
        __Records.DeleteOne(record => record.ID == id);


    public bool UpdateIndividual(IndividualEditRequestModel requestModel)
    {
        List<Record> _Records = Get();
        foreach (Record record in _Records)
        {
            IndividualModel _UpdatedIndividual = FindIndividualInTree(record, requestModel.ID);
            if (_UpdatedIndividual != null)
            {
                UpdateDetails(_UpdatedIndividual, requestModel);
                Update(record.ID, record);
                return true;
            }
        }

        return false;
    }

    private IndividualModel FindIndividualInTree(Record record, string id)
    {
        FamilyModel _RootFamily = record.Family.FirstOrDefault();
        if (_RootFamily.Father?.ID.ToString() == id)
        {
            return _RootFamily.Father;
        }
        else if (_RootFamily.Mother?.ID.ToString() == id)
        {
            return _RootFamily.Mother;
        }
        else
        {
            return FindIndividualInChildFamilies(_RootFamily, id);
        }
    }

    private IndividualModel FindIndividualInChildFamilies(FamilyModel family, string id)
    {
        foreach (FamilyModel childFamily in family.ChildrenFamily)
        {
            if (childFamily.Father?.ID.ToString() == id)
            {
                return childFamily.Father;
            }
            else if (childFamily.Mother?.ID.ToString() == id)
            {
                return childFamily.Mother;
            }
            else
            {
                return FindIndividualInChildFamilies(childFamily, id);
            }
        }
        return null;
    }

    public FamilyModel FindFamilyInChildFamiliesByIndividualID(FamilyModel family, string id)
    {
        foreach (FamilyModel childFamily in family.ChildrenFamily)
        {
            if (childFamily.Father?.ID.ToString() == id || childFamily.Mother?.ID.ToString() == id)
            {
                return childFamily;
            }
            else
            {
                FindFamilyInChildFamiliesByIndividualID(childFamily, id);
            }
        }
        return null;
    }

    public FamilyModel FindFamilyInChildFamiliesByFamilyIDForAdd(FamilyModel family, string id)
    {
        FamilyModel _MatchedFamily = null;

        if (family.ID.ToString() == id)
        {
            return family;
        }
        else
        {
            if (family.ChildrenFamily.Count > 0)
            {
                foreach (FamilyModel childFamily in family.ChildrenFamily)
                {
                    _MatchedFamily = FindFamilyInChildFamiliesByFamilyIDForAdd(childFamily, id);
                    if (_MatchedFamily != null)
                    {
                        return _MatchedFamily;
                    }
                }
            }
        }
        return _MatchedFamily;
    }

    public FamilyModel FindFamilyInChildFamiliesByFamilyID2ForReplace(FamilyModel family, string id)
    {
        FamilyModel _MatchedFamily = null;


        foreach (FamilyModel childFamily in family.ChildrenFamily)
        {
            if (childFamily.ID.ToString() == id)
            {
                return family;
            }
            else
            {
                _MatchedFamily = FindFamilyInChildFamiliesByFamilyID2ForReplace(childFamily, id);
                if (_MatchedFamily != null)
                {
                    return _MatchedFamily;
                }
            }
        }
        return null;
    }



    private void UpdateDetails(IndividualModel individual, IndividualEditRequestModel requestModel)
    {
        individual.Name = requestModel.Name;
        individual.Birth.Date = requestModel.DateOfBirth;
        individual.Death.Date = requestModel.DateOfDeath;
    }

    public bool AddIndividualToFamily(IndividualAddRequestModel requestModel)
    {
        //find the family
        List<Record> _Records = Get();
        FamilyModel _TargetFamily = new FamilyModel();
        foreach (Record record in _Records)
        {
            FamilyModel _RootFamily = record.Family.FirstOrDefault();
            if (_RootFamily.Father?.ID.ToString() == requestModel.TargetIndividualID
            || _RootFamily.Mother?.ID.ToString() == requestModel.TargetIndividualID)
            {
                _TargetFamily = _RootFamily;
            }
            else
            {
                _TargetFamily = FindFamilyInChildFamiliesByIndividualID(_RootFamily, requestModel.TargetIndividualID);
            }
            if (_TargetFamily != null)
            {
                FamilyModel _NewFamily = new FamilyModel();
                if (requestModel.Gender == "male")
                {
                    if (requestModel.IndividualType == "spouse")
                    {
                        _TargetFamily.Father = CreateIndividual(requestModel);
                    }
                    else if (requestModel.IndividualType == "child")
                    {
                        _NewFamily.Father = CreateIndividual(requestModel);
                        _TargetFamily.ChildrenFamily.Add(_NewFamily);

                    }
                }
                else if (requestModel.Gender == "female")
                {
                    if (requestModel.IndividualType == "spouse")
                    {
                        _TargetFamily.Mother = CreateIndividual(requestModel);
                    }
                    else if (requestModel.IndividualType == "child")
                    {
                        _NewFamily.Mother = CreateIndividual(requestModel);
                        _TargetFamily.ChildrenFamily.Add(_NewFamily);

                    }
                }


                Update(record.ID, record);
                return true;
            }
        }

        return false;
    }

    private IndividualModel CreateIndividual(IndividualAddRequestModel requestModel)
    {
        IndividualModel _NewIndividual = new IndividualModel();
        _NewIndividual.ID = ObjectId.GenerateNewId();
        _NewIndividual.Name = requestModel.Name;
        _NewIndividual.Birth = new BirthModel()
        {
            Date = requestModel.DateOfBirth,
            Place = requestModel.PlaceOfBirth
        };

        _NewIndividual.Death = new DeathModel()
        {
            Date = requestModel.DateOfDeath,
        };
        _NewIndividual.Gender = requestModel.Gender;
        _NewIndividual.IsAncestor = true;
        return _NewIndividual;
    }
}