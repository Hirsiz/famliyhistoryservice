using System.Collections.Generic;

public interface ISubmaintainerRepository
{
    List<Submaintainer> Get();
    Submaintainer Get(string id);
    Submaintainer Create(Submaintainer submaintainer);
    void Update(string id, Submaintainer submaintainerIn);
    void Remove(Submaintainer submaintainerIn);
    void Remove(string id);
}