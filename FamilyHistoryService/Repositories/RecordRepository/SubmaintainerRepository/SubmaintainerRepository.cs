using System.Collections.Generic;
using MongoDB.Driver;

public class SubmaintainerRepository : ISubmaintainerRepository
{
    private readonly IMongoCollection<Submaintainer> __Submaintainers;

    public SubmaintainerRepository(IFamilyTreeDatabaseSettings settings)
    {
        var client = new MongoClient(settings.ConnectionString);
        var database = client.GetDatabase(settings.DatabaseName);

        __Submaintainers = database.GetCollection<Submaintainer>(settings.SubmaintainerCollectionName);
    }

    public List<Submaintainer> Get() =>
        __Submaintainers.Find(group => true).ToList();

    public Submaintainer Get(string id) =>
        __Submaintainers.Find<Submaintainer>(submaintainer => submaintainer.Id == id).FirstOrDefault();

    public Submaintainer Create(Submaintainer submaintainer)
    {
        __Submaintainers.InsertOne(submaintainer);
        return submaintainer;
    }

    public void Update(string id, Submaintainer submaintainerIn) =>
        __Submaintainers.ReplaceOne(submaintainer => submaintainer.Id == id, submaintainerIn);

    public void Remove(Submaintainer submaintainerIn) =>
        __Submaintainers.DeleteOne(submaintainer => submaintainer.Id == submaintainerIn.Id);

    public void Remove(string id) =>
        __Submaintainers.DeleteOne(submaintainer => submaintainer.Id == id);

}