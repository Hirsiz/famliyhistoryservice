using System.Collections.Generic;

public interface IUserGroupRepository
{
    List<UserGroupModel> Get();
    UserGroupModel Get(string id);
}