using System.Collections.Generic;
using MongoDB.Driver;

public class UserGroupRepository : IUserGroupRepository
{
    private readonly IMongoCollection<UserGroupModel> __UserGroups;

    public UserGroupRepository(IFamilyTreeDatabaseSettings settings)
    {
        var client = new MongoClient(settings.ConnectionString);
        var database = client.GetDatabase(settings.DatabaseName);

        __UserGroups = database.GetCollection<UserGroupModel>(settings.UserGroupCollectionName);
    }

    public List<UserGroupModel> Get() =>
        __UserGroups.Find(group => true).ToList();

    public UserGroupModel Get(string id) =>
        __UserGroups.Find<UserGroupModel>(group => group.Id == id).FirstOrDefault();

}