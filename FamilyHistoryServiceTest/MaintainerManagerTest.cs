using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace FamilyHistoryServiceTest
{
    [TestClass]
    public class MaintainerManagerTest
    {
        private Mock<ISubmaintainerRepository> __SubmaintainerRepositoryMock;
        private Mock<IRecordRepository> __RecordRepositoryMock;


        public MaintainerManagerTest()
        {
            __SubmaintainerRepositoryMock = new Mock<ISubmaintainerRepository>();
            __RecordRepositoryMock = new Mock<IRecordRepository>();
        }

        private List<Submaintainer> GetSubmaintainers()
        {
            List<Submaintainer> _Submaintainers = new List<Submaintainer>(){
                new Submaintainer(){
                    Id = "id123",
                    TreeID = "treeId",
                    UserFamilySubmaintainers = new List<UserFamilyEntity>(){
                        new UserFamilyEntity(){
                            UserID = "15133",
                            Username = "Hirsiz",
                            FamilyName = "Jon Doe Jane Doe",
                            FamilyID = "241"
                        }
                    }
                }
            };
            return _Submaintainers;
        }


        [TestMethod]
        public void MaintainerManager_GetSubmaintainersForTree_ShouldReturnCorrectSubmaintainerFromTreeID()
        {
            __SubmaintainerRepositoryMock.Setup(mock => mock.Get()).Returns(GetSubmaintainers());
            Submaintainer _ActualSubmaintainer = new Submaintainer()
            {
                Id = "id123",
                TreeID = "treeId",
                UserFamilySubmaintainers = new List<UserFamilyEntity>(){
                        new UserFamilyEntity(){
                            UserID = "15133",
                            Username = "Hirsiz",
                            FamilyName = "Jon Doe Jane Doe",
                            FamilyID = "241"
                        }
                    }
            };

            MaintainerManager _MaintainerManager = new MaintainerManager(__RecordRepositoryMock.Object, __SubmaintainerRepositoryMock.Object);


            Submaintainer _TestSubmaintainer = _MaintainerManager.GetSubmaintainersForTree("treeId");
            Assert.AreEqual(_ActualSubmaintainer.TreeID, _TestSubmaintainer.TreeID);
            Assert.AreEqual(null, _TestSubmaintainer.Id);
        }
    }
}
